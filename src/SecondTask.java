import java.util.Arrays;

public class SecondTask {

	// Is the element on coordinates (row,col) valid in a matrix: (height)x(width)
	private static boolean isValidCell(int row, int col, int height, int width) {
		return row >= 0 && row < height && col >= 0 && col < width;
	}

	// Returns an array that contains all of the neighbours of an element in
	// position (row, col)
	// in a matrix: (height)x(width)
	public static int[] getSortedNeigbours(int[][] matrix, int height, int width, int row, int col) {
		int[] neighbours = new int[8];
		int neighboursCount = 0;
		// check all possible neighbours of a cell in matrix
		int[][] indexes = { { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, 1 }, { 1, 1 }, { 1, 0 }, { 1, -1 }, { 0, -1 } };

		for (int k = 0; k < indexes.length; k++) {
			if (isValidCell(row + indexes[k][0], col + indexes[k][1], height, width)) {
				neighbours[neighboursCount++] = matrix[row + indexes[k][0]][col + indexes[k][1]];
			}
		}
		// cuts the cells that were invalid
		neighbours = Arrays.copyOfRange(neighbours, 0, neighboursCount);
		Arrays.parallelSort(neighbours);
		return neighbours;
	}

	// Finds if a there is a number in matrix, whose neighbours make a valid
	// Fibonacci sequence (numbers don't have to be consecutive)
	public static boolean hasAnElementWithFibonacciNumberNeighbours(int[][] matrix) {
		int height = matrix.length;
		int width = matrix[0].length;

		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				if (FibonacciSequence.isFibonacciSequence(getSortedNeigbours(matrix, height, width, row, col)))
					return true;
			}
		}
		return false;
	}
}
