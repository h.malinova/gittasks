import org.junit.Test;

import static org.junit.Assert.*;

public class FibonacciSequenceTest {
	
    @Test
    public void isFibonacciNumber_Should_ReturnTrue_When_TheNumberIsFromFibonacciSequence() {
        assertTrue(FibonacciSequence.isFibonacciNumber(5));
        assertTrue(FibonacciSequence.isFibonacciNumber(13));
    }

    @Test
    public void isFibonacciNumber_Should_ReturnTrue_When_FirstTwoNumbersFromFibSequnceAreGiven() {
        assertTrue(FibonacciSequence.isFibonacciNumber(0));
        assertTrue(FibonacciSequence.isFibonacciNumber(1));
    }

    @Test
    public void isFibonacciNumber_Should_ReturnFalse_When_TheNumberIsNotFromFibonacciSequence() {
        assertFalse(FibonacciSequence.isFibonacciNumber(4));
        assertFalse(FibonacciSequence.isFibonacciNumber(14));
    }
    
    @Test
    public void areConsecutiveFibonacciNumbers_Should_ReturnTrue_When_FirstTwoFibNumbersFromFibSequnceAreGiven() {
        assertTrue(FibonacciSequence.areConsecutiveFibonacciNumbers(0, 1));
    }

    @Test
    public void areConsecutiveFibonacciNumbers_Should_ReturnTrue_When_SecondTwoFibNumbersFromFibSequnceAreGiven() {
        assertTrue(FibonacciSequence.areConsecutiveFibonacciNumbers(1, 1));
    }

    @Test
    public void areConsecutiveFibonacciNumbers_Should_ReturnFalse_When_OneIsFibNumberAndOneIsNotAreGiven() {
        assertFalse(FibonacciSequence.areConsecutiveFibonacciNumbers(3, 4));
        assertFalse(FibonacciSequence.areConsecutiveFibonacciNumbers(7, 13));
    }

    @Test
    public void areConsecutiveFibonacciNumbers_Should_ReturnTrue_When_TwoConsecutiveFibNumbersAreGiven() {
        assertTrue(FibonacciSequence.areConsecutiveFibonacciNumbers(13,21));
    }

    @Test
    public void areConsecutiveFibonacciNumbers_Should_ReturnFalse_When_TwoNonConsecutiveNumbersAreGiven() {
        assertFalse(FibonacciSequence.areConsecutiveFibonacciNumbers(5,13));
    }

    @Test
    public void isFibonacciSequence_Should_ReturnFalse_When_SwappedPairOfFiNumbersAreGiven() {
        assertFalse(FibonacciSequence.isFibonacciSequence(new int[] {2, 3, 5, 13, 8}));
    }

    @Test
    public void isFibonacciSequence_Should_ReturnFalse_When_NonFibNumberExistsInArray() {
        assertFalse(FibonacciSequence.isFibonacciSequence(new int[] {2, 3, 4, 5}));
        assertFalse(FibonacciSequence.isFibonacciSequence(new int[] {2, 3, 5, -1}));
    }

    @Test
    public void isFibonacciSequence_Should_ReturnFalse_When_ThereIsASkippedNumberFromFibSequence() {
        assertFalse(FibonacciSequence.isFibonacciSequence(new int[] {2, 3, 5, 13}));
    }

    @Test
    public void isFibonacciSequence_Should_ReturnFalse_When_ReversedFibSequenceIsGiven() {
        assertFalse(FibonacciSequence.isFibonacciSequence(new int[] {13, 8, 5}));
    }

    @Test
    public void isFibonacciSequence_Should_ReturnTrue_When_FibonacciSequenceIsGiven() { 
        assertTrue(FibonacciSequence.isFibonacciSequence(new int[] {2, 3, 5, 8, 13}));
        assertTrue(FibonacciSequence.isFibonacciSequence(new int[] {5, 8, 13, 21}));
    }


}