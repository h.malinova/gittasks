import static org.junit.Assert.*;

import org.junit.Test;

public class SecondTaskTest {
	
	@Test
	public void getSortedNeigbours_Should_CreateAnArray_When_ElementIsAtTheCorner() {
		int matrix[][] ={{  1,  2,  3,  4}, 
    					 {  5,  6,  7,  8}, 
    					 {  9, 10,  1,  0}, 
    					 { 13, 14,  1, 16}};
		int[] neighboursUpperLeft = { 2, 5, 6 };
		int[] neighboursUpperRight = { 3, 7, 8 };
		int[] neighboursLowerLeft = { 9, 10, 14 };
		int[] neighboursLowerRight = { 0, 1, 1 };
		assertArrayEquals(neighboursUpperLeft, SecondTask.getSortedNeigbours(matrix, 4, 4, 0, 0));
		assertArrayEquals(neighboursUpperRight, SecondTask.getSortedNeigbours(matrix, 4, 4, 0, 3));
		assertArrayEquals(neighboursLowerLeft, SecondTask.getSortedNeigbours(matrix, 4, 4, 3, 0));
		assertArrayEquals(neighboursLowerRight, SecondTask.getSortedNeigbours(matrix, 4, 4, 3, 3));
	}

	@Test
	public void getSortedNeigbours_Should_CreateAnArray_When_ElementIsAtTheMiddle() {
		int matrix[][] ={{  1,  2,  3,  4}, 
    					 {  5,  6,  7,  8}, 
    					 {  9, 10,  1,  0}, 
    					 { 13, 14,  1, 16}};
		int[] neighbours = { 1, 1, 2, 3, 5, 7, 9, 10 };
		assertArrayEquals(neighbours, SecondTask.getSortedNeigbours(matrix, 4, 4, 1, 1));
	}

	@Test
	public void getSortedNeigbours_Should_CreateAnArray_When_ElementIsAtTheBorder() {
		int matrix[][] ={{  1,  2,  3,  4}, 
    					 {  5,  6,  7,  8}, 
    					 {  9, 10,  1,  0}, 
    					 { 13, 14,  1, 16}};
		int[] neighbours = { 0, 1, 10, 14, 16 };
		assertArrayEquals(neighbours, SecondTask.getSortedNeigbours(matrix, 4, 4, 3, 2));
	}
	
	@Test
	public void hasAnElementWithFibonacciNumberNeighbours_Should_ReturnTrue_When_ThatElementExists() {
		int matrix[][] ={{ 9, 0, 9},
				         { 1, 1, 9},
						 { 9, 9, 9},
						 { 9, 9, 9}};
		assertTrue(SecondTask.hasAnElementWithFibonacciNumberNeighbours(matrix));
		
		int matrix2[][] ={	{ 9, 9,  9},
							{ 5, 8, 21},
							{ 3, 9, 13}};
		assertTrue(SecondTask.hasAnElementWithFibonacciNumberNeighbours(matrix2));
		
		int matrix3[][] ={	{ 2,   3,  5},
							{ 13,  9, 21},
							{ 34, 55,  8}};
		assertTrue(SecondTask.hasAnElementWithFibonacciNumberNeighbours(matrix3));
	}

	@Test
	public void hasAnElementWithFibonacciNumberNeighbours_Should_ReturnFalse_When_MatrixIsEmpty() {
		int matrix[][] = {{}};
		assertFalse(SecondTask.hasAnElementWithFibonacciNumberNeighbours(matrix));
	}

	@Test
	public void hasAnElementWithFibonacciNumberNeighbours_Should_ReturnFalse_When_ThatElementDoesntExist() {
		int matrix[][] ={	{ 0, 1, 1},
							{ 8, 9, 2},
							{ 5, 9, 3},
							{ 9, 9, 9}};
		assertFalse(SecondTask.hasAnElementWithFibonacciNumberNeighbours(matrix));
	}

	@Test
	public void hasAnElementWithFibonacciNumberNeighbours_Should_ReturnTrue_When_MatrixIsOneDimentional() {
		int matrix[][] ={{ 9, 1, 9}};
		assertTrue(SecondTask.hasAnElementWithFibonacciNumberNeighbours(matrix));
		int matrix2[][] ={{9}, {5}, {9}};
		assertTrue(SecondTask.hasAnElementWithFibonacciNumberNeighbours(matrix2));
	}
}
