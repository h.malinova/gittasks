import static org.junit.Assert.*;

import org.junit.Test;

public class FirstTaskTest {
	FirstTask test = new FirstTask();

	@Test
	public void spiralOrder_Should_CreateAnArray_When_MatrixHasEvenLength() {
		int[][] matrix = {  {  1,  2,  3,  4}, 
                			{  5,  6,  7,  8}, 
                			{  9, 10, 11, 12}, 
                			{ 13, 14, 15, 16}};
		int[] result = { 1, 2, 3, 4, 8, 12, 16, 15, 14, 13, 9, 5, 6, 7, 11, 10 };
		assertArrayEquals(result, test.spiralOrder(0, 0, 3, matrix));
	}

	@Test
	public void spiralOrder_Should_CreateAnArray_When_MatrixHasOddLength() {
		int[][] matrix = {  { 1, 2, 3}, 
                			{ 4, 5, 6}, 
                			{ 7, 8, 9}}; 
		int[] result = { 1, 2, 3, 6, 9, 8, 7, 4, 5 };
		assertArrayEquals(result, test.spiralOrder(0, 0, 2, matrix));

		int[][] matrix2 = { {  1,  2,  3,  4,  5}, 
							{  6,  7,  8,  9, 10}, 
							{ 11, 12, 13, 14, 15}, 
							{ 16, 17, 18, 19, 20},
							{ 21, 22, 23, 24, 25}};
		int[] result2 = { 1, 2, 3, 4, 5, 10, 15, 20, 25, 24, 23, 22, 21, 16, 11, 
						 6, 7, 8, 9, 14, 19, 18, 17, 12, 13 };
		assertArrayEquals(result2, test.spiralOrder(0, 0, 4, matrix2));
	}

	@Test
	public void spiralOrder_Should_CreateAnArray_When_MatrixHasEvenLengthAndDoesntStartAt0x0() {
		int[][] matrix = {  {  1,  2,  3,  4}, 
                			{  5,  6,  7,  8}, 
                			{  9, 10, 11, 12}, 
                			{ 13, 14, 15, 16}};
		int[] result = { 11, 12, 16, 15 };
		assertArrayEquals(result, test.spiralOrder(2, 2, 1, matrix));
	}

	@Test
	public void spiralOrder_Should_CreateAnArray_When_MatrixHasOddLengthAndDoesntStartAt0x0() {
		int[][] matrix = {  {  1,  2,  3,  4}, 
                			{  5,  6,  7,  8}, 
                			{  9, 10, 11, 12}, 
                			{ 13, 14, 15, 16}};
		int[] result = { 6, 7, 8, 12, 16, 15, 14, 10, 11 };
		assertArrayEquals(result, test.spiralOrder(1, 1, 2, matrix));
	}
	
	@Test
	public void isFibonacciSequence_Should_ReturnTrue_When_WantedMatrixTouchesCorners() {
		int[][] matrix = {  { 9, 9,  9,  9, 9},
							{ 4, 4,  4,  4, 4},
							{ 9, 9,  0,  1, 1},
							{ 4, 4, 13, 21, 2},
							{ 9, 9,  8,  5, 3}};
		assertTrue(test.isFibonacciSequenceInMatrix(matrix));
	}
	
	@Test
	public void isFibonacciSequence_Should_ReturnTrue_When_WantedMatrixIsInTheMiddleAndHasOddLength() {
		int[][] matrix = {  {9,  9,  9, 9, 9},
							{4,  0,  1, 1, 4},
							{9, 13, 21, 2, 5},
							{4,  8,  5, 3, 0},
							{9,  9,  8, 5, 3}};
		assertTrue(test.isFibonacciSequenceInMatrix(matrix));
	}
	
	@Test
	public void isFibonacciSequence_Should_ReturnTrue_When_WantedMatrixIsInTheMiddleAndHasEvenLength() {
		int[][] matrix = {  {9, 9,  9, 9, 9},
							{4, 0,  1, 1, 2},
							{9, 2,  1, 2, 5},
							{4, 8, 15, 3, 0},
							{9, 9,  8, 5, 3}};
		assertTrue(test.isFibonacciSequenceInMatrix(matrix));
	}

	@Test
	public void isFibonacciSequence_Should_ReturnTrue_When_WantedMatrixCoincidesWithGivenMatrix() {
		int[][] matrix = { { 0, 1 }, { 2, 1 } };
		assertTrue(test.isFibonacciSequenceInMatrix(matrix));
	}

	@Test
	public void isFibonacciSequence_Should_ReturnFalse_When_WantedMatrixCoincidesWithGivenMatrix() {
		int[][] matrix = { { 0, 1 }, { 4, 1 } };
		assertFalse(test.isFibonacciSequenceInMatrix(matrix));
	}
}
