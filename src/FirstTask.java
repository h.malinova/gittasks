public class FirstTask {
	// When given a matrix and coordinates of an element in it
	// Go "spiraly" through the matrix and save the elements
	// in the array: sequence
	int[] spiralOrder(int startRow, int startCol, int k, int[][] matrix) {
		int size = k + 1;
		int centerRow = k / 2 + startRow;
		int centerCol = k / 2 + startCol;
		int endRow = startRow + k;
		int endCol = startCol + k;

		int[] sequence = new int[size * size];
		int seqLength = 0;

		int fullCircumference = 0; // one full circumference of the borders
		int curRow = startRow;
		int curCol = startCol;
		while (curRow <= centerRow && curCol <= centerCol) {
			// go right
			for (; curCol < endCol - fullCircumference; curCol++) {
				sequence[seqLength] = matrix[curRow][curCol];
				seqLength++;
			}
			// go down
			for (; curRow < endRow - fullCircumference; curRow++) {
				sequence[seqLength] = matrix[curRow][curCol];
				seqLength++;
			}
			// go left
			for (; curCol > startCol + fullCircumference; curCol--) {
				sequence[seqLength] = matrix[curRow][curCol];
				seqLength++;
			}
			// go up
			for (; curRow > startRow + fullCircumference; curRow--) {
				sequence[seqLength] = matrix[curRow][curCol];
				seqLength++;
			}
			curRow++;
			curCol++;
			fullCircumference++;
		}
		if (size % 2 != 0) {
			sequence[seqLength] = matrix[--curRow][--curCol];
		}
		return sequence;
	}

	// Given a squared matrix, return if it contains a square that is having
	// Fibonacci sequence, starting form the upper left corner and going through
	// the square "spiraly"
	boolean isFibonacciSequenceInMatrix(int[][] matrix) {
		int size = matrix.length;
		for (int row = 0; row < size - 1; row++) {
			for (int col = 0; col < size - 1; col++) {
				for (int k = 1; k < size - Maximum.max(row, col); k++) {
					if (FibonacciSequence.isFibonacciSequence(spiralOrder(row, col, k, matrix))) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
