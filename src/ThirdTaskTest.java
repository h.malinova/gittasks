import static org.junit.Assert.*;

import org.junit.Test;

public class ThirdTaskTest {

	ThirdTask test = new ThirdTask();

	@Test
	public void findSumOfBits_Should_ReturnSumOfTheBits_When_EqualSumOfBits() {
		Integer[] input = { 8, 16, 4, 2 };
		test.sortByBitsInEachNumber(input);
		assertArrayEquals(new Integer[] { 2, 4, 8, 16 }, input);
	}

	@Test
	public void findSumOfBits_Should_Return0_When_GivenNumberIsZero() {
		assertEquals(0, test.findSumOfBits(0));
	}

	@Test
	public void findSumOfBits_Should_ReturnSumOfTheBits_When_OddNumbersAreGiven() {
		assertEquals(2, test.findSumOfBits(5));
		assertEquals(3, test.findSumOfBits(13));
	}

	@Test
	public void findSumOfBits_Should_ReturnSumOfTheBits_When_EvenNumbersAreGiven() {
		assertEquals(1, test.findSumOfBits(4));
		assertEquals(2, test.findSumOfBits(12));
	}

	@Test
	public void sortByBitsInEachNumber_Should_SortTheArrayByBits_When_MixedSumOfNumbers() {
		Integer[] input = { 5, 2, 3, 4 };
		test.sortByBitsInEachNumber(input);
		assertArrayEquals(new Integer[] { 2, 4, 3, 5 }, input);
	}

	@Test
	public void sortByBitsInEachNumber_Should_SortIt_When_DifferentSumOfBits() {
		Integer[] input = { 10, 7, 9, 5 };
		test.sortByBitsInEachNumber(input);
		assertArrayEquals(new Integer[] { 5, 9, 10, 7 }, input);
	}
}
