public class FibonacciSequence {

	// Checks if a number is part of the Fibonacci sequence
	public static boolean isFibonacciNumber(int a) {
		if (a == 0 || a == 1) {
			return true;
		}
		int first = 0;
		int second = 1;
		int third = 0;
		do {
			third = first + second;
			first = second;
			second = third;
		} while (third < a);
		if (third == a) {
			return true;
		}
		return false;
	}

	// Checks if two numbers are consecutive in the Fibonacci sequence
	public static boolean areConsecutiveFibonacciNumbers(int a, int b) {
		if (a == 0 && b == 1 || a == 1 && b == 1) {
			return true;
		}
		int first = 0;
		int second = 1;
		int third = 0;
		do {
			third = first + second;
			first = second;
			second = third;
		} while (third < a);
		if (third == a && first + second == b) {
			return true;
		}
		return false;
	}

	// Checks if an array is a Fibonacci sequence
	public static boolean isFibonacciSequence(int[] sequence) {
		if (sequence.length == 1) {
			return isFibonacciNumber(sequence[0]);
		}

		int first = sequence[0];
		int second = sequence[1];
		if (areConsecutiveFibonacciNumbers(first, second)) {
			for (int pos = 2; pos < sequence.length; pos++) {
				if (sequence[pos] != first + second)
					return false;
				first = second;
				second = sequence[pos];
			}
			return true;
		} else {
			return false;
		}
	}
}
