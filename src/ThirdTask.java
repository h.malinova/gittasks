import java.util.Arrays;
import java.util.Comparator;

public class ThirdTask {
	void sortByBitsInEachNumber(Integer[] inputArray) {
		Arrays.parallelSort(inputArray, new Comparator<Integer>() {
			@Override
			public int compare(Integer a, Integer b) {
				int bitsOfa = findSumOfBits(a);
				int bitsOfb = findSumOfBits(b);
				if (bitsOfa != bitsOfb) {
					return bitsOfa - bitsOfb;
				}
				return a - b;
			}
		});
	}

	int findSumOfBits(int num) {
		int sum = 0;
		while (num != 0) {
			if (num % 2 == 1) {
				sum++;
			}
			num /= 2;
		}
		return sum;
	}
}
